# resources unittest

import logging
import pytest

# import IncubatorServiceResource, PlateReaderServiceResource, MoverServiceResource
from pythonlab.resource import ServiceResource, LabwareResource, DynamicLabwareResource, SubstanceResource


class TestProcess:
    def register_service_resource(self, resource):
        pass

    def register_labware_resource(self, resource):
        pass


def test_ServiceResource():
    tp = TestProcess()
    sr = ServiceResource(proc=tp, name="TestServiceResource")
    assert sr.name == "TestServiceResource"


def test_LabwareResource():
    tp = TestProcess()
    lr = LabwareResource(proc=tp, name="TestLabwareResource")
    assert lr.name == "TestLabwareResource"


def test_DynamicLabwareResource():
    tp = TestProcess()
    dlr = LabwareResource(proc=tp, name="TestDynamicLabwareResource")
    assert dlr.name == "TestDynamicLabwareResource"


# test importing of all resources
@pytest.mark.skip(reason="not implemented yet ")
def test_importResources():
    #inc_res = IncubatorServiceResource()
    #pl_res = PlateReaderServiceResource()
    #mv_res = MoverServiceResource()
    #assert mv_res.init() == "init movers"
    pass
