Installation of the Implementation
===================================


git clone https://foss.heptapod.net/pypy/pypy

# or if you have a public ssh key:
cat id_rsa.pub 
git clone ssh://hg@foss.heptapod.net/pypy/pypy
git branch

git checkout -b  branch/sandbox-2  origin/branch/sandbox-2

cd sandbox/
ll
apt install libffi-dev pkg-config libzstd-dev libbz2-dev libexpat1-dev libunwind-dev 
apt install libsqlite3-dev libssl-dev libncursesw5-dev libgdbm-dev tk-dev liblzma-dev 
apt install libgc-dev python-cffi
apt install zlib1g-dev libbz2-dev
cd ..
cd pypy/goal/

apt install libncurses5-dev
apt-get install gcc make libffi-dev pkg-config zlib1g-dev libbz2-dev libsqlite3-dev libncurses5-dev libexpat1-dev libssl-dev libgdbm-dev tk-dev libgc-dev python-cffi liblzma-dev libncursesw5-dev 
sudo apt-get install gcc make libffi-dev pkg-config zlib1g-dev libbz2-dev libsqlite3-dev libncurses5-dev libexpat1-dev libssl-dev libgdbm-dev tk-dev libgc-dev python-cffi liblzma-dev libncursesw5-dev 
pypy ../../rpython/bin/rpython --opt=jit

git status
git checkout branch/default
pypy ../../rpython/bin/rpython --opt=jit
# worked
mv pypy-c pypy-c_jit_mdo

git checkout branch/sandbox-2
ll
mv libpypy-c.so libpypy-c.so_jit_mdo
pypy ../../rpython/bin/rpython -O2 --sandbox targetpypystandalone
git log
git branch

# building worked
mv pypy-c pypy-sandbox
ll
cd ..
cd goal/
micro hello.py
./pypy-sandbox hello.py 
./pypy-c_jit_mdo hello.py 
cd ..
cd sandbox/
ll
pypy_interact.py ../goal/pypy-sandbox 
./pypy_interact.py ../goal/pypy-sandbox 

cd tool/release/

python package.py 
du -hs ../../goal/

mkdir  $HOME/temp/python/pypy-sandbox
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal --targetdir $HOME/temp/python/pypy-sandbox
cd ..
ll
cd goal/
ll
cp pypy-sandbox pypy-c
cd ..
cd tool/release/
ll
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal --targetdir $HOME/temp/python/pypy-sandbox
pypy package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal --targetdir $HOME/temp/python/pypy-sandbox
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal --targetdir $HOME/temp/python/pypy-sandbox
python
ll $HOME/temp/python/pypy-sandbox
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal 
cd ..
cd release/
code package.py 

cd sandbox/
pypy_interact.py path/to/pypy-sandbox
ll
./pypy_interact.py path/to/pypy-sandbox
echo $PYTHONPATH
export PYTHONPATH=/lair/source/collection/python/pypy/site-packages/setuptools
./pypy_interact.py path/to/pypy-sandbox
export PYTHONPATH=/lair/source/collection/python/pypy/rpython
./pypy_interact.py path/to/pypy-sandbo
./pypy_interact.py ../goal/pypy-sandbox 
git branch
git status
git log
./pypy_interact.py ../goal/pypy-sandbox 
cd ..
cd pypy/tool/release/
ll
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal 
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal --without-tk  --without-pwdgrp 
cd ..
ll
cd ..
mkdir pypy_
mv pypy pypy_/
mv pypy_/ pypy
cd pypy
git clone ssh://hg@foss.heptapod.net/pypy/sandboxlib
ll
cd sandboxlib/
ll
python3.6 -m venv ~/py3venv/pypy-sandbox
source ~/py3venv/pypy-sandbox/bin/activate
pip install .
ll
less setup.py 
cd ..
cd pypy/pypy/pypy/sandbox/
ll
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal  --targetdir ~/temp/pypy
cd ..
ll
cd pypy/tool/release/
python package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal  --targetdir ~/temp/pypy
python2.7 package.py --archive-name pypy3.6-v7.3.0rc4-sandbox-linux64 --builddir ../../goal  --targetdir ~/temp/pypy
cd ..
ll
cd sandbox/
ll
./pypy_interact.py ../goal/pypy-sandbox 
ll
pypy_interact.py ../goal/pypy-sandbox 
./pypy_interact.py ../goal/pypy-sandbox 
less ./pypy_interact.py 
python2.7
micro pypy_interact.py 
./pypy_interact.py ../goal/pypy-sandbox 
deactivate
cd ..
cd sandboxlib/
ll
apt install virtualenv
virtualenv -p python2.7 ~/py2venv/pypy-sandbox2
source ~/py2venv/pypy-sandbox2/bin/activate
ll
pip install .
source ~/py2venv/pypy-sandbox2/bin/activate
python
micro pypy_interact.py 
./pypy_interact.py ../goal/pypy-sandbox 
micro pypy_interact.py 
./pypy_interact.py ../goal/pypy-sandbox 
micro pypy_interact.py 
./pypy_interact.py ../goal/pypy-sandbox 
cd
cd  ../goal/pypy-sandbox 
cd ..
cd pypy/pypy/goal/
cd ..
ll
cd sandboxlib/
ll
./interact.py ../pypy/pypy/goal/pypy-sandbox 
ll
less setup.py 
pip2.7 install cffi
#python setup.py build_ext -f -i
ll
python setup.py build_ext -f -i
./interact.py ../pypy/pypy/goal/pypy-sandbox 
code .bash_history 