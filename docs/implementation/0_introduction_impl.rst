Implementation of pythonLib
=============================

The current implementation proposes the following pipeline:

1. The lab automation process is formulated in a synthax subset of python 3.6
   These processes can contain any valid python expressions and data tpyes.
1. The execution of this script is done in a safe (=sandboxed) environment.
   This means that, e.g. all access to system calls (like writing to the file system) 
   are prohibited by the sandbox environment.
1. The script in the sandbox environment communicates with a process manager 
   (=a python script with more execution rights) through controlled and safe channels.
1. The process manager does the controlled execution of the code.

There are several ways to achieve a python sandbox environment, like, e.g.  `SECCOMP <http://code.google.com/p/seccompsandbox/wiki/overview>`_  on Linux or
`gVisor <https://gvisor.dev/>`_  (A bad idea would be to restrict the OS acces of the CPython libraries, as discussed here ...)

A very elegant and relativly safe approach is using pypy-sandbox (in its 2nd version), since the risks of attacs is substantially reduced as explained in:
`Title <http://link>`_ .

Currently pypy-sandbox-2 installation is not straight forward. 
(This might change, when pypy-sandbox documentation will be more complete and compiled packages will be made available ).
For a comprehensive guide, please refer to 1_installation_impl_ .

 
