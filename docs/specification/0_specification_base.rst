
pythonLab Core Specification
=============================

The **pythonLab Specification** specifies only the Syntax of the 
laboratory process description language (LPDL).
The implementation and selection of runtime environment is free of choice. 

To keep this specification lean and flexible, it is devided into two parts:
The *Core language syntax definition* and *Extensions*. In the Core no lab process related topics are specified.
The Extensions are *standardised*  **base classes** with defined names and a defined set of class variables, 
which can be used by every implementer to derive further functionality. To maintain reproducibility it is important 
that all relevant details of the implementation are also documented in standardised parameter modules.


PythonLab Base / Core
----------------------

Syntax Specification
_____________________

**pythonLab** follows the Python 3.6 Syntax as announced in `PEP 494 <https://www.python.org/dev/peps/pep-0494/>`_ and `Python 3.6 - what's new <https://docs.python.org/3/whatsnew/3.6.html>`_ .
and specified in `Full Grammar specification. <https://docs.python.org/3/reference/grammar.html>`_ .

The implempentation could be arbitrary. 

It is RECOMMENDED to follow `PEP 8 Style Guide <https://www.python.org/dev/peps/pep-0008/>`_ writing new code.

The Core can be flexible extended by Extensions. These are base classes with standardised class names.

(The reason for chosing the python 3.6 subset is related to the implementation development of pypy-sandbox: pypy-sandbox currently supports python 3.6 syntax.)


Resources
----------
An important concept in pythonLab are **resources**. **Resources** are all phyiscal objects
for which the process describes interactions with.
Resources are modeled as python classes. 

New Resources can be added by importing them as modules from any location of the file system.
A base set of Resources is provided by pythonLab as "plugin modules" = any resources definition in the default resources folder will be included in the core resources.

Resources have methods

Examples for Resources:
  - containers
  - devices and container positions in or at a device

  mover = DeviceResource()
  incubator = DeviceResource()
  cont1 = ContainerResource()
  mover.move(cont1, source=incubator.pos1, target=incubator.pos2 )

Constraints
-----------
**Constraints** can be used to specify all kind of numeric and flow constraints.

  * parameter constraints
    - numeric constraints
  * flux constraints
    - determine, if a next step needs to be directly followed by the current step

Extensions
-----------


Since the design is very open, it can be easily extended by any other feature.
(Suggestions and contributions in form of merge/pull requests are very welcome :) 


Container definition and handling
__________________________________




Implementation Parameter Modules 
----------------------------------

Although the pythonLab Standard specifies nothing about the implementation, it is REQUIRED to document all necessary details of the implementation parameters to reproduce a certain operation, e.g. by an exact description of the physical process.
[Details on the format of these Parameter Modules are under discussion.]


Keyword Conventions
--------------------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" 
in this document are to be interpreted as described in IETF RFC 2119.