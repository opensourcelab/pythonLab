pythonLab Extensions
=====================

pythonLab is very modular by design.
The core can be extended. 

Module specification
----------------------
All modules are subclasses of the ProcessStep base class...


Standard Extensions for common processes are provided by pythonLab, like


Liquid Handling
________________



Mixing-Shaking
________________


Temperature-Control
____________________


Data-Evaluation
_________________

