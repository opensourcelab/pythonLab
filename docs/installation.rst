.. highlight:: shell

============
Installation
============


Stable release
--------------

To install pythonLab, run this command in your terminal:

.. code-block:: console

    $ pip install pythonlab

This is the preferred method to install pythonLab, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

