Coding Style guidlines
=======================

For *consistancy and better readability* of the code and not confusing the readers with too many
different styles, we would like to ask you to follow a few rules when submitting code to the **SiLA_python** repository, which are briefly:

  * please use the recommendations of `PEP8 Style Guide <https://www.python.org/dev/peps/pep-0008>`_ for code formatting, but some differences are acceptable:
  * please use a maximum of 120 characters per line
  * to differntiate between a variable name and a mothod name,  camelCased module names are also acceptable (e.g. `myExcitingNewMethod()` )
  * please use tools like `Pylint <https://www.pylint.org/>`_ or Pylama to check your code readability
  * some function names might differ from the default, since all feature access functions are predefined in the SiLA standard and thus use CamelCase naming.
  * be careful with the new python 3 **format strings** ( `f"the content of variable my_var is:  {my_var}"` ):
    please only use them if the refering variable definition is visible within **approx. 20 lines** above.

Further reading:

  - `PEP8 at real python <https://realpython.com/python-pep8/>`_
  - `python guide <https://docs.python-guide.org/writing/style/>`_



package names file names

documentation file header functions/methods classes

sphinx
