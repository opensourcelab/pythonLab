creating the package
====================

::

    cd [dir containing pack setup]
    python3 setup.py sdist

Installation
--------------
::

    pip3 install --user pack





creating the PyPi distribution
--------------------------------

see https://packaging.python.org/tutorials/packaging-projects/

-  create virtual env (recommended)
-  install pypi tools
 
.. code-block::

   pip install --upgrade pip 
   pip install --upgrade wheel setuptools  twine

-  Now run this from the same directory where setup.py is
   located:

.. code-block::

   python3 setup.py sdist bdist_wheel

-  run Twine to upload all of the archives under dist:

.. code-block::

   twine upload --repository-url https://test.pypi.org/legacy/ dist/\*

-  testing the installation (installing from
   https://test.pypi.org/simple/)

.. code-block::

   pip3 install --index-url https://test.pypi.org/simple/ labpy

-  if tests are successful, install everything for real ....

.. code-block::

   twine upload dist/\*
