"""_____________________________________________________________________

:PROJECT: pythonLab

*process - process related classes.*

:details: this module definces process related base classes.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging

class RunExternalProtocol(ProcessStep):
    def __init__(self, target_device=None, nest_position=None, lidded="U", orientation="L", protocol="", duration=30.0, container=[]):
        super(RunExternalProtocol, self).__init__(step_device=target_device,  lidded=lidded, orientation=orientation, container=container, duration=duration)
        
        self.protocol_name = protocol
        
    def protocol(self):
        return(self.protocol_name)

