"""_____________________________________________________________________

:PROJECT: pythonLab

*shake - shaking related base classes.*

:details: This module defines shaking related base classes.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging

class Shake(ProcessStep):    
    def __init__(self, device=None, duration=60.0, mode="Shake", container=None):
        super(Shake, self).__init__(step_device=device, lidded=False, container=container)
        
        self.step_duration = duration