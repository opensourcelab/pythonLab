"""_____________________________________________________________________

:PROJECT: pythonLab

*incubation - incubation related base classes.*

:details: This module defines incubation related base classes.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging

class Centrifuge(ProcessStep):
    def __init__(self, device=None, lidded="U", orientation="L", protocol="", speed=0, duration=180.0, temperature=0.0,  container=[]):
        super(Centrifuge, self).__init__(step_device=device,  lidded=lidded, orientation=orientation, duration=duration, container=container)
         
        self.centr_speed = speed
        self.centr_temperature = temperature
        
    def speed(self):
        return(self.centr_speed)
        
    def temperature(self):
        return(self.centr_temperature)