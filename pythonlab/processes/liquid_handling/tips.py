"""_____________________________________________________________________

:PROJECT: pythonLab

*tips - tip handling related base classes.*

:details: This module defines tip handling related base classes.
          - handling of static / mounted tips 
          - handling of disposable tips
          - handling of needles

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging

class LoadTips(ProcessStep):
    def __init__(self, target_device=None, position=0, lidded="U", orientation="L", columns=12, rows=8, column_pos=0, row_pos=0, static=0, container=[]):
        super(LoadTips, self).__init__(step_device=target_device,  lidded=lidded, orientation=orientation, container=container)
        self.plate_pos = position
        self.pos_static = static
        
        self.step_duration = 20.0
        
        self.column_pos = column_pos
        self.row_pos = row_pos

        self.columns_to_load = columns
        self.rows_to_load = rows
        
    def columnPos(self):
        return(self.column_pos)
        
    def rowPos(self):
        return(self.row_pos)
        
    def columns(self):
        return(self.columns_to_load)
        
    def rows(self):
        return(self.rows_to_load)
        
        
class UnloadTips(ProcessStep):
    def __init__(self, target_device=None, position=0, lidded="U", orientation="L", columns=12, rows=8, column_pos=0, row_pos=0, static=0, container=[]):
        super(UnloadTips, self).__init__(step_device=target_device,  lidded=lidded, orientation=orientation, container=container)
        self.plate_pos = position
        self.pos_static = static
        
        self.step_duration = 20.0

        self.column_pos = column_pos
        self.row_pos = row_pos
        
        self.columns_to_unload = columns
        self.rows_to_load = rows
        
    def columnPos(self):
        return(self.column_pos)
        
    def rowPos(self):
        return(self.row_pos)
        
    def columns(self):
        return(self.columns_to_unload)
        
    def rows(self):
        return(self.rows_to_unload)


class TipsOn(ProcessStep):
    def __init__(self, device=None, container=None, nest_position=None):
        super(TipsOn, self).__init__(step_device=device, container=container)
        self.plate_pos = nest_position
        
        self.step_duration = 10.0
                
    def platePosition(self):
        return(self.plate_pos)
        
class TipsOff(ProcessStep):
    def __init__(self, device=None, container=None, nest_position=None):
        super(TipsOff, self).__init__(step_device=device, container=container)
        self.plate_pos = nest_position
        
        self.step_duration = 10.0
                
    def platePosition(self):
        return(self.plate_pos)