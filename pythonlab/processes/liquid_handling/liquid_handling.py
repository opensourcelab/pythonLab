"""_____________________________________________________________________

:PROJECT: pythonLab

*liquid_handling - liquid handling related base classes.*

:details: This module defines liquid handling related base classes.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging

class Aspirate(ProcessStep):    
    def __init__(self, device=None, mode="Aspirate", volume=0, prime_volume=10, liquids_channel=1, column_pos=12, row_pos=8, container_type="Greiner96NwFb", container=None):
        super(Aspirate, self).__init__(step_device=device, lidded=False, container=container)
        
        self.step_duration = 180.0
        
        # modes = Dispense, Empty, Prime
        self.dispense_mode = mode
        self.container_type = container_type
        self.aspirate_volume = volume
        self.prime_volume = prime_volume
        self.liquids_channel = liquids_channel
        self.std_prime_volume = 3000
        self.std_empty_volume = 3000
        
        logging.info("should be replaced by basis class LiquidHandling")
        self.column_to_aspirate = column_pos
        self.row_to_aspirate = row_pos
        
    def mode(self):
        return(self.dispense_mode)
        
    def contType(self):
        return(self.container_type)
        
    def primeVolume(self):
        return(self.prime_volume)
            
    def emptyVolume(self):
        if self.aspirate_volume :
            return(self.aspirate_volume)
        else:
            return(self.std_empty_volume)    
            
    def volume(self):
        if self.aspirate_volume :
            return(self.aspirate_volume)
        else:
            return(0)

    def liquidsChannel(self):
        return(self.liquids_channel)
        
    def columnPos(self):
        return(self.column_to_aspirate)
        
    def rowPos(self):
        return(self.row_to_aspirate)
        
class Dispense(ProcessStep):    
    def __init__(self, device=None, mode="Dispense", volume=0, prime_volume=10, liquids_channel=1, column_pos=12, row_pos=8,  container_type="Greiner96NwFb", container=None):
        super(Dispense, self).__init__(step_device=device, lidded=False, container=container)
        
        self.step_duration = 180.0
        
        # modes = Dispense, Empty, Prime
        self.dispense_mode = mode
        self.container_type = container_type
        self.dispense_volume = volume
        self.prime_volume = prime_volume
        self.liquids_channel = liquids_channel
        self.std_prime_volume = 3000
        self.std_empty_volume = 3000
        
        
        self.column_to_dispense = column_pos
        self.row_to_dispense = row_pos
        
    def mode(self):
        return(self.dispense_mode)
        
    def contType(self):
        return(self.container_type)
        
    def primeVolume(self):
        return(self.prime_volume)
            
    def emptyVolume(self):
        if self.dispense_volume :
            return(self.dispense_volume)
        else:
            return(self.std_empty_volume)    
            
    def volume(self):
        if self.dispense_volume :
            return(self.dispense_volume)
        else:
            return(0)

    def liquidsChannel(self):
        return(self.liquids_channel)
    
    def columnPos(self):
        return(self.column_to_dispense)
        
    def rowPos(self):
        return(self.row_to_dispense)


class Mix(ProcessStep):    
    def __init__(self, device=None, mode="Dispense", volume=0, prime_volume=10, liquids_channel=1, column_pos=12, row_pos=8, container_type="Greiner96NwFb", container=None):
        super(Mix, self).__init__(step_device=device, lidded=False, container=container)
        
        self.step_duration = 180.0
        
        # modes = Dispense, Empty, Prime
        self.dispense_mode = mode
        self.container_type = cont_type
        self.mix_volume = volume
        self.prime_volume = prime_volume
        self.liquids_channel = liquids_channel
        self.std_prime_volume = 3000
        self.std_empty_volume = 3000
        
        self.column_to_mix = columns
        self.row_to_mix = row_pos
        
    def mode(self):
        return(self.dispense_mode)
        
    def primeVolume(self):
        return(self.prime_volume)
            
    def emptyVolume(self):
        if self.mix_volume :
            return(self.mix_volume)
        else:
            return(self.std_empty_volume)    
            
    def volume(self):
        if self.mix_volume :
            return(self.mix_volume)
        else:
            return(0)

    def liquidsChannel(self):
        return(self.liquids_channel)
        
    def columnPos(self):
        return(self.column_to_mix)
        
    def rowPos(self):
        return(self.row_to_mix)
