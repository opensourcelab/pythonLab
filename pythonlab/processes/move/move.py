"""_____________________________________________________________________

:PROJECT: pythonLab

*move - move object related base classes.*

:details: This module defines move object related base classes.
          - moving containers / labware
          - moving lids
          - moving tools
          - loading robot decs

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"
     
class MovePlate(ProcessStep):
    def __init__(self, target_device=None, position=0, lidded="U", orientation="L", static=0, container_type="Greiner96NwFb", container=[]):
        super(MovePlate, self).__init__(step_device=target_device,  lidded=lidded, orientation=orientation, container=container)
        self.plate_pos = position
        self.pos_static = static
        
        self.container_type = container_type
        
        self.step_duration = 20.0
    
    def platePosition(self):
        if self.plate_pos == 0:
            return("")
        else:
            return(str(self.plate_pos))
            
    def isStatic(self):
        return(str(self.pos_static))
        
    def contType(self):
        return(self.container_type)

class AutoLoad(ProcessStep):
    def __init__(self, device=None, lidded="U", orientation="L", container=None):
        super(AutoLoad, self).__init__(step_device=device, lidded=lidded, orientation=orientation, container=container)
        self.step_duration = 20.0