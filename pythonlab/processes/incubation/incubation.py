"""_____________________________________________________________________

:PROJECT: pythonLab

*incubation - incubation related base classes.*

:details: This module defines incubation related base classes.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200319

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging

# to do refine !


class Incubate(ProcessStep):
    def __init__(self, device=None, container=None, nest_position=None, temperature=37, incubation_duration=0, duration=60.0, OD=0.7):
        super(Incubate, self).__init__(step_device=device,
                                       container=container, duration=duration)

        self.step_duration = duration + float(incubation_duration * 60)
        self.incubation_duration = incubation_duration

    def incubationDuration(self):
        return(self.incubation_duration)
