"""_____________________________________________________________________

:PROJECT: pythonLab

*process base class*

:details: 

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20210410

________________________________________________________________________

"""

__version__ = "0.0.3"

import logging
from enum import Enum
from graphviz import Digraph
import networkx as nx

from abc import ABC, abstractmethod
# should be done through plugin system
#from pythonlab.resource import ServiceResource, LabwareResource


class ExecutionConstraint(Enum):
    immediate = 1
    parallel = 2


class PLProcess(ABC):
    def __init__(self, priority=10):
        self._priority = priority  # 0 has the highest priority
        self._service_resources = []
        self._container_resources = []
        self._substance_resources = []
        self._data_resources = []
        # indicates that the samples have to started in the same order,as they are added
        self.preserve_order = False

        self.workflow = nx.DiGraph()  # this graph will represent the whole experiment
        self.container_nodes = {}  # dict container_name --> container_start_node

        self._service_resources = []
        self.create_resources()
        self.init_service_resources()

    @abstractmethod
    def create_resources(self):
        raise NotImplementedError

    @abstractmethod
    def init_service_resources(self):
        """calling all init routines.
           If no additional code is required, 
           this can be called, using
           super().init_resources() in an overwriting method.
        """
        for resource in self._service_resources:
            resource.init()

    # @abstractmethod
    # def set_starting_position(self, resource: LabwareResource, device: ServiceResource, position: int):
    #     """
    #     This method gets called when setting the starting position of a container resource. It exists to be overwritten.
    #     :param resource:
    #     :param device:
    #     :param position:
    #     :return:
    #     """
    #     # SM
    #     raise NotImplementedError

    @abstractmethod
    def add_process_step_priorities(self):
        """we add waiting costs to all jobs, so these will be prioritised by scheduler
           should be also add waiting costs before the start, so they will be started first?

          SM
        :raises NotImplementedError: _description_
        """
        raise NotImplementedError

    @abstractmethod
    def process(self):
        raise NotImplementedError

    def add_node(self, attr):
        n = self.workflow.number_of_nodes()
        self.workflow.add_nodes_from([(n, attr)])
        return n

    def add_edge(self, s, t, **kwargs):
        self.workflow.add_edges_from([(s, t, kwargs)])

    def register_service_resource(self, resource):
        logging.debug(f"reg service. res: {resource.name}")
        self._service_resources.append(resource)

    def register_container_resource(self, resource):
        logging.debug(f"reg cont. res: {resource.name}")
        self._container_resources.append(resource)

    def register_substance_resource(self, resource):
        logging.debug(f"reg subst res: {resource.name}")
        self._substance_resources.append(resource)

    def register_data_resource(self, resource):
        logging.debug(f"reg data res: {resource.name}")
        self._data_resources.append(resource)

    def shutdown_resources(self):
        for resource in self._service_resources:
            resource.shutdown()

    @property
    def service_resources(self):
        return self._service_resources

    @property
    def container_resources(self):
        return self._container_resources

    @property
    def substance_resources(self):
        return self._substance_resources

    @property
    def data_resources(self):
        return self._data_resources

    @property
    def priority(self):
        return self._priority
