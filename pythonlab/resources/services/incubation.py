
from pythonlab.resources.resource import ServiceResource, Position


class IncubatorServiceResource(ServiceResource):
    """
      multi-cavity / single cavity ?
      associated containers, like lids, stacks

    :param Resource: [description]
    :type Resource: [type]
    """

    def __init__(self,
                 proc,
                 name: str,
                 capacity: int):
        super().__init__(proc=proc, name=name)
        self._capacity = capacity

        self._transfer_positon = Position(self, 0)

    @property
    def capacity(self):
        return self._capacity

    @property
    def transfer_position(self):
        return self._transfer_positon

    def init(self):
        logging.debug("iniit incubator")

    def incubate(self, container, duration, temperature, shaking_frequency=100, **kwargs):
        logging.debug(
            f"Incubator: incubate {container.name} for {duration} at T={temperature}")
        kwargs.update(dict(fct='incubate', duration=duration-20, temperature=temperature,
                           frequency=shaking_frequency))
        self.proc.add_action(ActionType.process, self, container, **kwargs)


class IncubatorServiceResourcePool(IncubatorServiceResource):
    def __init__(self,
                 proc,
                 name: str = "incubator_pool",
                 capacity: int = 32):
        super().__init__(proc=proc, name=name)
