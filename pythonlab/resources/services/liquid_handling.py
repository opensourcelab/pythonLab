from pythonlab.resources.resource import ServiceResource, Position


class LiquidHandlerServiceResource(ServiceResource):
    """


    :param Resource: [description]
    :type Resource: [type]
    """

    def __init__(self, proc, name: str, capacity: int):
        super().__init__(proc=proc, name=name)
        self.capacity = capacity
        self._transfer_positon = Position(self, position=0)

    @property
    def transfer_position(self):
        return self._transfer_positon

    def init(self):
        logging.debug("i-init liquid handler")

    def executeProtocol(self, container: ContainerResource, protocol: str, **kwargs):
        logging.debug(f"\n execute {protocol} on {container.name}.\n")
        # bring all needed reagents to the liquid handler
        last_job = {}
        if "reagents" in kwargs:
            for i, reagent in enumerate(kwargs['reagents']):
                if isinstance(reagent, ReagentResource):
                    # copy their current position, so we can later reset them
                    last_job[reagent.name] = self.proc.last_job[reagent.name].copy()
                    action_kwargs = dict(
                        resource=self.proc.mover_pool, container=reagent, fct='move', target=type(self))
                    if "reagent_pos" in kwargs:
                        preferred_pos = kwargs["reagent_pos"]
                        if preferred_pos[i] is not None:
                            action_kwargs['position'] = preferred_pos[i]
                    # we assume reagent_pos to be a dictionary[reagent.name -> position]
                    if 'reagent_pos' in kwargs:
                        if reagent.name in kwargs['reagent_pos']:
                            action_kwargs['position'] = kwargs['reagent_pos'][reagent.name]
                    self.proc.add_action(ActionType.move, **action_kwargs)
                    reagent.wait_cost(reagent.outside_cost)

        if "duration" not in kwargs:
            kwargs['duration'] = 60
        kwargs.update(dict(fct='executeProtocol', method=protocol))
        self.proc.add_action(ActionType.process, self, container, **kwargs)

        # bring all needed reagents back to their storage
        if "reagents" in kwargs:
            for reagent in kwargs['reagents']:
                if isinstance(reagent, ReagentResource):
                    reagent.wait_cost(reagent.outside_cost)
                    self.proc.add_action(ActionType.move, resource=self.proc.mover_pool, container=reagent, fct='move',
                                         target=type(
                                             reagent.start_position.resource),
                                         executor=[
                                             reagent.start_position.resource],
                                         position=reagent.start_position.pos)
                    # reset the reagents las job
                    self.proc.last_job[reagent.name] = last_job[reagent.name]

 #self.liquid_handler_pool = LiquidHandlerServiceResource(proc=self, name="liquid_handler_pool", capacity=9)
