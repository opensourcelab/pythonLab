from pythonlab.resources.resource import ServiceResource, Position


class MoverServiceResource(ServiceResource):
    """
     mover 

    :param Resource: [description]
    :type Resource: [type]
    """

    def __init__(self, proc, name: str, capacity: int = 1):
        self._capacity = capacity
        super().__init__(proc=proc, name=name)

    def init(self):
        logging.debug("init mover")

    # stefan: i plan not to use the source location. that should be saved internally
    def move(self, labware: LabwareResource, target_loc: ServiceResource, **kwargs):
        logging.debug(
            f"moving {labware.name} from {labware.pos} to {target_loc.name}\n")
        kwargs.update(dict(fct='move', target=type(target_loc)))
        self.proc.add_action(ActionType.move, self, labware, **kwargs)

    def read_barcode(self, labware: labwareResource, **kwargs):
        kwargs.update(dict(fct="read_barcode", duration=50))
        self.proc.add_action(ActionType.process, self, labware, **kwargs)

 #self.mover_pool = MoverServiceResource(proc=self, name="mover_pool")
