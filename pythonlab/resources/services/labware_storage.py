
from pythonlab.resources.resource import ServiceResource, Position


class LabwareStorageResource(ServiceResource):
    """
      multi-cavity / single cavity ?
      associated containers, like lids, stacks

    :param Resource: [description]
    :type Resource: [type]
    """

    def __init__(self,
                 proc,
                 name: str,
                 capacity: int):
        super().__init__(proc=proc, name=name)

        self._capacity = capacity

        self._free_positions = {1, 3, 4, 9}  # hack - will be dynamically
        self._next_free_position = self.get_next_free_position()

        self._start_position = Position(self, 0)

    @property
    def capacity(self):
        return self._capacity

    @property
    def next_free_position(self):
        return next(self._next_free_position)

    def init(self):
        logging.debug("iniit container storage")

    def get_container(self, container):
        logging.debug(
            f"fetching container {container.name} from positon {container.pos}")

    def get_next_free_position(self):
        for free_pos in self._free_positions:
            yield Position(self, free_pos)


class ContainerStorageResourcePool(LabwareStorageResource):
    pass

 #self.hotel_pool = ContainerStorageResource(proc=self, name="hotel_pool", capacity=200)
