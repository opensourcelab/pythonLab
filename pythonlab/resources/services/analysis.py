
import logging
from pythonlab.resources.resource import ServiceResource, Position


class PlateReaderServiceResource(ServiceResource):
    """
      multi-cavity / single cavity ?
      associated containers, like lids, stacks

    :param Resource: [description]
    :type Resource: [type]
    """

    def __init__(self,
                 proc,
                 name: str):
        super().__init__(proc=proc, name=name)

        self._average_aborbance = 0

    def init(self):
        logging.debug(f"iniit {self.name}")

    def single_read(self, container, wavelengths=None, temperature=305, method='single_read', **kwargs):
        # a rough heuristic to estimate the duration
        if wavelengths is None:
            wavelengths = [600]
        if "duration" not in kwargs:
            kwargs['duration'] = 20+20*len(wavelengths)
        kwargs.update(dict(fct='absorbance', temperature=temperature,
                           wavelengths=wavelengths, method=method))
        self.proc.add_action(ActionType.process, self, container, **kwargs)

    def run_kinetic(self, container, wavelength, interval, reads, temperature=305, **kwargs):
        kwargs.update(dict(fct='absorbance', duration=reads*interval+30, temperature=temperature,
                           wavelength=wavelength, interval=interval, reads=reads))
        self.proc.add_action(ActionType.process, self, container, **kwargs)

    def run_series(self, container, protocols: List[str], **kwargs):
        """runs more than one protocol"""
        # make a rough estimate for the duration
        if "duration" not in kwargs:
            kwargs['duration'] = 60*len(protocols)
        kwargs.update(dict(fct='read_series', protocols=protocols))
        self.proc.add_action(ActionType.process, self, container, **kwargs)

    def run_process(self, process_name, **kwargs):
        logging.debug(f"Absorbance: {process_name}")
        self._average_aborbance = 2.1

    def average_aborbance(self):
        logging.debug(f"Average Absorbance: {self._average_aborbance}")
        return self._average_aborbance

 #self.reader_pool = PlateReaderServiceResource(proc=self, name="reader_pool")
