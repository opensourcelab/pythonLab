from pythonlab.resources.resource import ServiceResource, Position


class CentrifugeServiceResource(ServiceResource):
    """
      Centrifuge resource

    :param Resource: [description]
    :type Resource: [type]
    """

    def __init__(self, proc, name: str, capacity: int):
        super().__init__(proc=proc, name=name)
        self.capacity = capacity
        self._transfer_positon = Position(self, position=0)

    @property
    def transfer_position(self):
        return self._transfer_positon

    def init(self):
        logging.debug("i-init centrifuge")

    def centrifuge(self, containers: list, duration: int, rpm: int, **kwargs):
        logging.debug(
            f"\n centrifuge {containers} for {duration} s at rpm {rpm}\n")
        kwargs.update(dict(fct='centrifuge', duration=duration, rpm=rpm))
        for container in containers:
            self.proc.add_action(ActionType.process, self, container, **kwargs)


#self.centrifuge_pool = CentrifugeServiceResource(proc=self, name="centrifuge_pool", capacity=4)
