
# making a convenient interface
# s. https://stackoverflow.com/questions/3365740/how-to-import-all-submodules

import importlib
import pkgutil
from inspect import isclass
import logging

from pythonlab.resources.services.analysis import *
from pythonlab.resources.services.labware_storage import *
from pythonlab.resources.services.incubation import *
from pythonlab.resources.services.moving import *
from pythonlab.resources.services.centrifugation import *


# def import_submodules(package, recursive=True):
#     """ Import all submodules of a module, recursively, including subpackages

#     :param package: package (name or actual module)
#     :type package: str | module
#     :rtype: dict[str, types.ModuleType]
#     """
#     if isinstance(package, str):
#         package = importlib.import_module(package)
#     results = {}
#     for loader, module_name, is_pkg in pkgutil.walk_packages(package.__path__):
#         full_name = package.__name__ + '.' + module_name
#         results[full_name] = importlib.import_module(full_name)
#         if recursive and is_pkg:
#             results.update(import_submodules(full_name))
#     return results


# import_submodules(__name__)

# for attribute_name in dir(full_name):
#             attribute = getattr(package, attribute_name)
#             print(attribute_name)

#         if isclass(attribute):
#             globals()[attribute_name] = attribute
