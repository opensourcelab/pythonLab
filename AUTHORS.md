
# Acknowledgements and Credits

The pythonLab project thanks


Contributors
------------

* Stefan Maak <stefan.maak@uni-greifswald.de>
* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* mark doerr <mark.doerr@uni-greifswald.de>
