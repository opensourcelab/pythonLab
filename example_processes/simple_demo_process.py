from pythonlab.resource import IncubatorServiceResource, PlateReaderServiceResource
from pythonlab.process import PLProcess


class DemoProcess(PLProcess):
    def __init__(self,
                 priority=10):  # !! highest priority
        super().__init__(priority=priority)

    def create_resources(self):
        self.incubator1 = IncubatorServiceResource(
            proc=self, name="incubator1", capacity=16)
        self.plate_reader1 = PlateReaderServiceResource(
            proc=self, name="plate_reader1")

    def init_service_resources(self):
        super().init_service_resources()

    def process(self):

        max_aborbance = 2.0
        incubation_duration = 6000
        process_name = "OD606"

        self.incubator1.incubate(
            container="cont1", duration=incubation_duration)
        self.plate_reader1.run_process(process_name)

        curr_absorbance = self.plate_reader1.average_aborbance()
        if curr_absorbance < max_aborbance:
            #a = 4
            self.incubator1.incubate(
                container="cont1", duration=incubation_duration)
            b = 4
            # if a > 3:
            #    b = 3
            #    print(a)
